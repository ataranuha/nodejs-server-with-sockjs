/**
 * ============== NodeJS Push server ==============
 * User: ataranuha
 * Create: 28.03.13
 */

// ------ CONFIGURATION ------

const _SERVER_PORT = 8888;
const _LOG_ENABLED = true;

// ------ MODULES ------

var http = require('http');
var sockjs = require('sockjs');
var events = require('events');

var trades = require('./trades.js');
var url = require('url');
var express = require('express');

// ------ CREATE SERVER ------

var em = new events.EventEmitter();
em.setMaxListeners(0);
var app = express();
var server = http.createServer(app);

server.listen(_SERVER_PORT); //set server port

var sockjs_echo = sockjs.createServer();
sockjs_echo.installHandlers(server, {prefix: '/sockjs'});

app.use(express.bodyParser());

// ------ POST FUNCTION ------

app.post('/post*', function (req, res) {
    var data = Object.keys(req.body);
    var url = req.url; // /post?id=12
    var pos = url.indexOf("?id=")+4;
    var id = url.substr(pos);
    var chanel = 'push:'+id;
    em.emit(chanel,data);
    res.send("");
});

// ------ SOCKJS CONNECTION ------

sockjs_echo.on('connection', function (conn) {
    conn.write('{"response":"Connection success"}');

    // get message form clients
    conn.on('data', function (message) {
        var msg = JSON.parse(message);

        if (msg.cmd == 'buy' || msg.cmd == 'sell') {
            trades.trade(msg);

        } else if (msg.channels && msg.channels.length>0) {
            conn.channels = msg.channels;
            for (var key in conn.channels) {
                var chanel = 'push:'+conn.channels[key];
                console.log('set push listener as '+chanel);
                em.on(chanel, conn.listener);
            }
        } else {
            conn.close();
        }
    });

    // disconnect if we didn't get message with channels from client
    setTimeout(function(){
        if (!(conn.channels && conn.channels.length>0)) {
            conn.close();
        };
    },3000);

    conn.listener = function (data) {
        conn.write(data);
    };

    // remove listeners after disconnect
    conn.on('close', function () {
        if (conn.channels && conn.channels.length>0) {
            for (var key in conn.channels) {
                var chanel = 'push:'+conn.channels[key];
                em.removeListener(chanel,conn.listener);
            }
        }
    });
});

console.log(' [*] Listening on 0.0.0.0:'+_SERVER_PORT);

trades.emiter = em;
trades.events = events;
trades.randomTrade(); //start trading

if (_LOG_ENABLED) {
    //remove log function
    console.log=function(){};
}


