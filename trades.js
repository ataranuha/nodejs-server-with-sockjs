module.exports = {
    emiter: false,
    max_value: 1000,
    min_value: 0,
    current_value: 500,
    emitChanel: 'push:trades_arena',
    init: function () {
        this.id = Math.random();
        console.log(this.id);
    },
    setCurrentValue: function (data) {
        this.current_value = data.value;
        if (this.emiter) {
            var date = new Date().getTime();
            var response = {
                'type': 'sysInfo',
                'response': {
                    'price': data.value,
                    'listeners': this.events.EventEmitter.listenerCount(this.emiter, this.emitChanel),
                    'timestamp': date,
                    'username': data.username,
                    'diff': data.diff
                }
            };
            this.emiter.emit(this.emitChanel, JSON.stringify(response));
        }
    },
    trade: function (data) {
        var value = data.value;
        console.log('trade: ' + parseInt(value)+' | new value: ' + data.value+' | user: ' + data.username);
        data.diff = parseInt(value);
        data.value = this.current_value + parseInt(value);
        if (data.value < this.max_value && data.value > this.min_value) {
            this.setCurrentValue(data);
        }
    },
    randomTrade: function () {
        var randomInterval = 1000;
        var that = this;
        setTimeout(function () {
            var randomValue = Math.round((Math.random() * 100) - 50);
            var data = {
                value: randomValue,
                username: '--'
            }
            that.trade(data);
            that.randomTrade();
        }, randomInterval);
    }
}